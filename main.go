package main

import (
	"fmt"
	"wikipediaCrawler/initCrawl"
	"wikipediaCrawler/mongoDriver"
	"wikipediaCrawler/wiki"
)

func main() {
	initCrawl.ReadConfig()
	client := mongoDriver.Connect(initCrawl.CrawlerConfig.MongoAddr)
	defer mongoDriver.Disconnect(client)
	mainPageColl := client.Database("wikipedia").Collection("MainPage")
	DocsColl := client.Database("wikipedia").Collection("Docs")

	fmt.Println("wikipedia scraper")
	mainPageChan := make(chan wiki.TodayMainPage, 10)
	docChan := make(chan wiki.WikiDocParagraph, 10)
	urlPool := make(chan string, 10000)
	contentPool := make(chan string, 100)

	go wiki.FetchMainPage(mainPageChan)
	entry := "https://en.wikipedia.org/wiki/watch"
	go wiki.FetchDocs(contentPool, urlPool, entry, docChan)
	wiki.InsertDB(mainPageChan, docChan, mainPageColl, DocsColl)

}
