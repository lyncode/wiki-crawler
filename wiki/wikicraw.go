package wiki

import (
	"context"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"go.mongodb.org/mongo-driver/mongo"
	"log"
	"math/rand"
	"net/http"
	"strings"
	"time"
)

type TodayMainPage struct {
	TimeStamp time.Time
	MainPage  map[string][]string
}

type WikiDocParagraph struct {
	Link      string
	Keyword   string
	Paragraph string
}

func httpGet(url string) *goquery.Document {
	res, err := http.Get(url)
	if err != nil {
		log.Println(err)
	}
	defer res.Body.Close()
	if res.StatusCode != http.StatusOK {
		log.Printf("status code error: %d %s", res.StatusCode, res.Status)
	}
	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(res.Body)

	if err != nil {
		log.Println(err)
	}
	return doc
}

func unique(a []string) []string {
	b := make(map[string]string)
	c := make([]string, 0)
	for _, ele := range a {
		b[ele] = ele
	}
	for key := range b {
		c = append(c, key)
	}
	return c
}

func WikiUrl(entry string, urlPool chan string) {
	if len(urlPool) > 50 {
		return
	}
	links := make([]string, 0)
	doc := httpGet(entry)
	doc.Find("a[href]").Each(func(i int, s *goquery.Selection) {
		link, _ := s.Attr("href")
		if strings.Contains(link, ":") ||
			strings.Contains(link, ".") ||
			strings.Contains(link, "#") {
			return
		}
		links = append(links, "https://en.wikipedia.org"+link)
	})
	links = unique(links)
	for i := range links {
		j := rand.Intn(i + 1)
		links[i], links[j] = links[j], links[i]
	}
	for i := 0; i < len(links); i++ {
		urlPool <- links[i]
	}
}

func WikiMainPage() map[string][]string {
	// Request the HTML page.
	doc := httpGet("https://en.wikipedia.org/wiki/Main_Page")
	var inThisDay, oldTheDay, didYouKnow []string
	mainPageContent := make(map[string][]string)
	doc.Find("div.itn-img~ul li").Each(func(i int, s *goquery.Selection) {
		inThisDay = append(inThisDay, s.Text())
	})
	doc.Find("div#mp-otd-img~ul li").Each(func(i int, s *goquery.Selection) {
		oldTheDay = append(oldTheDay, s.Text())
	})
	doc.Find("div#mp-dyk-img~ul li").Each(func(i int, s *goquery.Selection) {
		didYouKnow = append(didYouKnow, s.Text())
	})
	mainPageContent["inThisDay"] = inThisDay
	mainPageContent["oldTheDay"] = oldTheDay
	mainPageContent["didYouKnow"] = didYouKnow
	return mainPageContent
}

func WikiDoc(link string, contentPool chan string, urlPool chan string) (string, string) {
	defer func() { // 必须要先声明defer，否则不能捕获到panic异常
		if err := recover(); err != nil {
			fmt.Println(err) // 这里的err其实就是panic传入的内容
		}
	}()

	doc := httpGet(link)
	paragraph := ""
	for i := 3; i < 10; i++ {
		selector := fmt.Sprintf("#mw-content-text > div > p:nth-child(%v)", i)
		paragraph = doc.Find(selector).Text()
		switch {
		case paragraph != "":
			break
		case paragraph == "\n" || paragraph == "":
			continue
		}
	}
	contentPool <- paragraph
	WikiUrl(link, urlPool)
	firstParagraph := <-contentPool
	if firstParagraph == "" {
		return "", ""
	}
	//fmt.Println(link, firstParagraph)
	return link, firstParagraph
}

func FetchMainPage(mainPageChan chan TodayMainPage) {
	for {
		PageContent := WikiMainPage()
		todayPage := TodayMainPage{time.Now(), PageContent}
		mainPageChan <- todayPage
		time.Sleep(time.Hour * 24)
	}
}

func FetchDocs(contentPool chan string, urlPool chan string, entry string, docChan chan WikiDocParagraph) {
	WikiUrl(entry, urlPool)
	for {
		time.Sleep(60 * time.Second)
		newUrl := <-urlPool
		if newUrl == "" {
			continue
		}
		link, paragraph := WikiDoc(newUrl, contentPool, urlPool)
		if paragraph == "" {
			continue
		}
		keyword := strings.SplitN(link, "/wiki/", 2)[1]
		firstParagraph := WikiDocParagraph{link, keyword, paragraph}
		docChan <- firstParagraph
	}
}

func InsertDB(mainPageCHan chan TodayMainPage, docChan chan WikiDocParagraph,
	mainPageColl *mongo.Collection, DocsColl *mongo.Collection) {
	for {
		select {
		case mainPage := <-mainPageCHan:
			insertResult, err := mainPageColl.InsertOne(context.TODO(), mainPage)
			if err != nil {
				log.Fatal(err)
			}
			fmt.Println("Inserted a MainPage : ", insertResult.InsertedID)
		case doc := <-docChan:
			insertResult, err := DocsColl.InsertOne(context.TODO(), doc)
			if err != nil {
				log.Fatal(err)
			}
			fmt.Println("Inserted a single document: ", insertResult.InsertedID)
		}
	}

}
