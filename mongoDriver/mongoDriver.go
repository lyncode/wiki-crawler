package mongoDriver

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
)

func Connect(Address string) *mongo.Client {
	clientOptions := options.Client().ApplyURI(Address)
	//clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		func() {
			err = client.Disconnect(context.TODO())
			if err != nil {
				log.Fatal(err)
			}
			fmt.Println("Connection to MongoDB closed.")
		}()
		log.Fatal(err)
	}
	if err = client.Ping(context.TODO(), nil); err != nil {
		log.Fatal(err)
	} // Check the connection
	fmt.Println("Connected to MongoDB!")
	return client
}

func Disconnect(client *mongo.Client) {
	err := client.Disconnect(context.TODO())
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("Connection to MongoDB closed.")
}
