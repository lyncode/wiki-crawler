FROM golang:1.12.7-alpine3.9 AS builder
MAINTAINER LYN "lynmeig@live.com"
WORKDIR /go/src/wikipediaCrawler
RUN apk update && apk upgrade && apk add git
ADD . .
RUN go get github.com/golang/dep/cmd/dep
RUN dep init && dep ensure
RUN pwd && ls
RUN go build main.go

# multi stage build
FROM alpine:latest as prod
#RUN pwd && ls
RUN apk --no-cache add ca-certificates
WORKDIR /wikipediaCrawler
#RUN pwd && ls
COPY  --from=builder /go/src/wikipediaCrawler/ /wikipediaCrawler/
#RUN pwd && ls
ENTRYPOINT ["./main"]

