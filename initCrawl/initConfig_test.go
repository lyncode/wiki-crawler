package initCrawl

import (
	"log"
	"testing"
	"wikipediaCrawler/mongoDriver"
)

func TestReadConfig(t *testing.T) {
	ReadConfig()
	if CrawlerConfig.MongoAddr != "mongodb://mongo-0.mongo:27017" {
		t.Fatalf("test fail")
	}
	client := mongoDriver.Connect("mongodb://localhost:27017")
	if client == nil {
		log.Fatal("connect to db fail")
	}
}
