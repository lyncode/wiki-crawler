package initCrawl

import (
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
)

type Config struct {
	MongoAddr  string `yaml:"MongoAddr"`
	EntryPoint string `yaml:"EntryPoint"`
}

var CrawlerConfig Config

func ReadConfig() {
	file, err := ioutil.ReadFile("config/wikipedia.yaml")
	if err != nil {
		log.Fatalf("Read Config File fail %s", err)
	}
	err = yaml.Unmarshal(file, &CrawlerConfig)
}
